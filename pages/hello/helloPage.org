#+TITLE: Welcome!
#+AUTHOR:yam
#+EMAIL:toyangmin@gmail.com
#+DATE:Thu Mar 12 17:16:19 2015

#+OPTIONS: ^:nil ':t *:t f:t |:t <:t toc:nil H:3 num:nil
#+HTML_HEAD: <link rel="stylesheet" href="../style/base.css" />

* 欢迎
  #+ATTR_HTML: :alt welcome image :title Welcome! :align center :width 200px
  [[./img/Dice.png]]

  #+BEGIN_QUOTE
  既来之则安之
  #+END_QUOTE
  

  #+BEGIN_HTML
  <!-- 多说评论框 start -->
  <div class="ds-thread" data-thread-key="d:/HOME/blog/pages/hello/helloPage.org}" data-title="helloPage.org" data-url="http://dunner.bitbucket.org/pages/hello/helloPage.html"></div>
  <!-- 多说评论框 end -->
  <!-- 多说公共JS代码 start (一个网页只需插入一次) -->
  <script type="text/javascript">
  var duoshuoQuery = {short_name:"dunner"};
  (function() {
  var ds = document.createElement('script');
  ds.type = 'text/javascript';ds.async = true;
  ds.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') + '//static.duoshuo.com/embed.js';
  ds.charset = 'UTF-8';
  (document.getElementsByTagName('head')[0] 
  || document.getElementsByTagName('body')[0]).appendChild(ds);
  })();
  </script>
  <!-- 多说公共JS代码 end -->
  #+END_HTML
